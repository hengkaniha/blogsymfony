# Blog Symfony

Projet de site de blog permettant à un artiste de publier ses oeuvres.

## Environnement de développement

### Pré-requis

* PHP 7.4
* Composer
* Symfony CLI
* Docker
* Docker-compose
* Nodejs
* npm

```bash
composer install
npm install
npm run build
docker compose up -d
```

### Ajouter des données de tests (phpFaker)

```bash
symfony console doctrine:fixtures:load
```

## Se mettre dans le conteneur docker (/var/www)

```bash
docker exec -it www_docker_symfony bash
```

## URL

### Site web

localhost:8080

### MailDev

localhost:8081

### phpMyAdmin

localhost:8082

## Lancer des tests

```bash
php bin/phpunit --testdox
```

## Production

### Envoie des mails de contacts

Les mails de prise de contact sont stockés en BDD, pour les envoyeer à l'artiste, il faut mettre en place un CRON sur :

Se mettre dans le conteneur docker avant de faire la commande.

```bash
symfony console app:send-contact
```