<?php

namespace App\Controller;

use App\Entity\Oeuvre;
use App\Entity\Commentaire;
use App\Form\CommentaireType;
use App\Service\CommentaireService;
use App\Repository\OeuvreRepository;
use App\Repository\CommentaireRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class OeuvreController extends AbstractController
{
    /**
     * @Route("/realisations", name="realisations")
     */
    public function realisations(
        OeuvreRepository $oeuvreRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $data = $oeuvreRepository->findBy([], ['id' => 'DESC']);

        $oeuvres = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            6
        );
        return $this->render('oeuvre/realisations.html.twig', [
            'oeuvres' => $oeuvres,
        ]);
    }

    /**
     * @Route("/realisations/{slug}", name="realisations_details")
     */
    public function details(
        Oeuvre $oeuvre,
        Request $request,
        CommentaireService $commentaireService,
        CommentaireRepository $commentaireRepository
    ): Response {
        $commentaires = $commentaireRepository->findCommentaires($oeuvre);
        $commentaire = new Commentaire();
        $form = $this->createForm(CommentaireType::class, $commentaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $commentaire = $form->getData();
            $commentaireService->persistCommentaire($commentaire, null, $oeuvre);

            return $this->redirectToRoute('realisations_details', ['slug' => $oeuvre->getSlug()]);
        }
        
        return $this->render('oeuvre/details.html.twig', [
            'oeuvre' => $oeuvre,
            'form' => $form->createView(),
            'commentaires' => $commentaires,
        ]);
    }
}
