<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;

class AProposController extends AbstractController
{
    /**
     * @Route("/a-propos", name="a_propos")
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('a_propos/index.html.twig', [
            'artiste' => $userRepository->getArtiste(),
        ]);
    }
}
