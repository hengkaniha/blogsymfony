<?php

namespace App\EventSubscriber;

use DateTime;
use App\Entity\Oeuvre;
use App\Entity\Blogpost;
use App\Entity\Categorie;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;

class EasyAdminSubscriber implements EventSubscriberInterface
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['setDateAndUser'],
        ];
    }

    public function setDateAndUser(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if ($entity instanceof Categorie) {
            return;
        }
        
        if (!($entity instanceof Oeuvre) || !($entity instanceof Blogpost)) {
            $now = new DateTime('now');
            $entity->setCreatedAt($now);
    
            $user = $this->security->getUser();
            $entity->setUser($user);
        }

        return;
    }
}
