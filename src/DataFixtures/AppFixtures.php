<?php

namespace App\DataFixtures;

use App\Entity\Blogpost;
use App\Entity\Categorie;
use App\Entity\Oeuvre;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
* @codeCoverageIgnore
*/
class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        // Utilisation de Faker
        $faker = Factory::create('fr_FR');

        // Création d'un User
        $user = new User();

        $user->setEmail('user@test.com')
             ->setPrenom($faker->firstName())
             ->setNom($faker->lastName())
             ->setTelephone($faker->phoneNumber())
             ->setAPropos($faker->text())
             ->setInstagram('instagram')
             ->setRoles(['ROLE_ARTISTE']);

        $password = $this->encoder->encodePassword($user, 'password');
        $user->setPassword($password);

        $manager->persist($user);

        // Création de 10 blogpost
        for ($i = 0; $i < 10; $i++) {
            $blogpost = new Blogpost();

            $blogpost->setTitre($faker->words(3, true))
                     ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                     ->setContenu($faker->text(350))
                     ->setSlug($faker->slug(3))
                     ->setUser($user);

            $manager->persist($blogpost);
        }

        // Création d'un Blogpost pour les tests
        $blogpost = new Blogpost();

        $blogpost->setTitre('Blogpost test')
                 ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                 ->setContenu($faker->text(350))
                 ->setSlug('blogpost-test')
                 ->setUser($user);

        $manager->persist($blogpost);

        // Création de 5 Catégories
        for ($k = 0; $k < 5; $k++) {
            $categorie = new Categorie();

            $categorie->setNom($faker->word())
                      ->setDescription($faker->words(10, true))
                      ->setSlug($faker->slug());

            $manager->persist($categorie);
            // Création de 2 oeuvres/catégories
            for ($j = 0; $j < 2; $j++) {
                $oeuvre = new Oeuvre();
                $oeuvre->setNom($faker->words(3, true))
                    ->setLargeur($faker->randomFloat(2, 20, 60))
                    ->setHauteur($faker->randomFloat(2, 20, 60))
                    ->setEnVente($faker->randomElement([true, false]))
                    ->setDateRealisation($faker->dateTimeBetween('-6 month', 'now'))
                    ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                    ->setDescription($faker->text())
                    ->setPortfolio($faker->randomElement([true, false]))
                    ->setSlug($faker->slug())
                    ->setFile('placeholder.jpg')
                    ->addCategorie($categorie)
                    ->setPrix($faker->randomFloat(2, 100, 9999))
                    ->setUser($user);
                $manager->persist($oeuvre);
            }
        }

        // Catégorie de test
        $categorie = new Categorie();

        $categorie->setNom('categorie test')
                  ->setDescription($faker->words(10, true))
                  ->setSlug('categorie-test');

        $manager->persist($categorie);

        // Oeuvre pour les tests
        $oeuvre = new Oeuvre();

        $oeuvre->setNom('oeuvre test')
            ->setLargeur($faker->randomFloat(2, 20, 60))
            ->setHauteur($faker->randomFloat(2, 20, 60))
            ->setEnVente($faker->randomElement([true, false]))
            ->setDateRealisation($faker->dateTimeBetween('-6 month', 'now'))
            ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
            ->setDescription($faker->text())
            ->setPortfolio($faker->randomElement([true, false]))
            ->setSlug('oeuvre-test')
            ->setFile('placeholder.jpg')
            ->addCategorie($categorie)
            ->setPrix($faker->randomFloat(2, 100, 9999))
            ->setUser($user);
    
        $manager->persist($oeuvre);
        $manager->flush();
    }
}
