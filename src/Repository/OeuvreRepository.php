<?php

namespace App\Repository;

use App\Entity\Oeuvre;
use App\Entity\Categorie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Oeuvre|null find($id, $lockMode = null, $lockVersion = null)
 * @method Oeuvre|null findOneBy(array $criteria, array $orderBy = null)
 * @method Oeuvre[]    findAll()
 * @method Oeuvre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OeuvreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Oeuvre::class);
    }

    /**
    * @return Oeuvre[] Returns an array of Oeuvre objects
    */
    public function lastThree()
    {
        return $this->createQueryBuilder('p')
                    ->orderBy('p.id', 'DESC')
                    ->setMaxResults(3)
                    ->getQuery()
                    ->getResult()
        ;
    }

    /**
     * @return Oeuvre[] Returns an array of Oeuvre objects
     */
    public function findAllPortfolio(Categorie $categorie): array
    {
        return $this->createQueryBuilder('p')
             ->where(':categorie MEMBER OF p.categorie')
             ->andWhere('p.portfolio = TRUE')
             ->setParameter('categorie', $categorie)
             ->getQuery()
             ->getResult()
         ;
    }
}
