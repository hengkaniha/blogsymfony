<?php

namespace App\Service;

use DateTime;
use App\Entity\Oeuvre;
use App\Entity\Blogpost;
use App\Entity\Commentaire;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class CommentaireService
{
    private $manager;
    private $flash;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flash)
    {
        $this->manager = $manager;
        $this->flash = $flash;
    }

    public function persistCommentaire(
        Commentaire $commentaire,
        Blogpost $blogpost = null,
        Oeuvre $oeuvre = null
    ): void {
        $commentaire->setIsPublished(false)
                    ->setBlogpost($blogpost)
                    ->setOeuvre($oeuvre)
                    ->setCreatedAt(new DateTime('now'));
        $this->manager->persist($commentaire);
        $this->manager->flush();
        $this->flash->add('success', 'Votre commentaire est bien envoyé. Il sera publié après vérification du contenu.');
    }
}
